
var chalk = require("chalk");
exports.ui = class inface {
    constructor() 
    {
        var events = require('events');
        this.events = new events();
        this.config = require("./config.json")
        this.index = 0;
        this.row = 0;
        this.selected = 0;
 
          this.data = {
            mItems: [["Main", "Config","New","Logs"]],
            submenus: {
                Main:[["Start bot"],["Reload"], ["exit"]],
                Config:[],
                Logs:[],
                New:[["command"]]

            }
        }
        Object.keys(this.config).forEach((i) => {
            this.data.submenus.Config.push([[i],[this.config[i]]])
        })
        this.paused = false;
         this.render();
    }

    navigate(dir)
    {
        if(this.row > 0) {
            if(dir == "left") this.selected--
            else this.selected++
            return this.render()
        }

        if(dir === "left")
        {
           this.index--;
        } else {
           this.index++;
        }
        this.render()
        
    }

    pause(state) {
        this.paused = state;
    }
    reload() {
        delete require.cache[require.resolve("./config.json")]
        var data =  require("./config.json")
        this.config = data;
        this.data.submenus.Config = [];
        Object.keys(this.config).forEach((i) => {
            this.data.submenus.Config.push([[i],[this.config[i]]])
        })
    }
    start()
    {
        var keypress = require('keypress');
        keypress(process.stdin);
        
       
            let _this = this;
            process.stdin.on('keypress', function (ch, key) {
                {
                    if(_this.paused) return;
                   switch(key.name)
                   {
                       case "left":
                           _this.navigate("left")
                           break;
                       case "right":
                            _this.navigate("right")
                       break;
                       case "up":
                            _this.row--
                            _this.render()
                       break;
                       case "down":
                            _this.row++
                            _this.render()
                       break;
                       case "return":
                           let bb = _this.data.mItems[0][_this.index]
                           let ee = _this.data.submenus[bb]
                         
                           let sender = {
                               rowinfo:{
                                   index: _this.index,
                                   row: _this.row,
                                   selected: _this.selected
                               },
                               selected_row: {
                                   index:bb,
                           row: ee[_this.row-1],
                                   item: ee[_this.row-1][_this.selected]
                               }
                           }
                            _this.events.emit("return", sender)
                       break;
                   }
                }
    
              });
        


          
          process.stdin.setRawMode(true);
          process.stdin.resume();
    }

    





    render()
    {
        var table = require("table");
        let d = JSON.parse(JSON.stringify(this.data))
        let _data = d.mItems[0];
        
        console.clear();
        if(this.row <= 0)
        {
            if(_data[this.index]) {
                let str = _data[this.index]
                _data[this.index] = chalk.underline(str)
            } else {
                this.index = 0;
                this.render()
            } 
            console.log(`LDclient, VER 1.0.3`)
            console.log(table.table([_data]))
        } else {
            if(!d.submenus[_data[this.index]]||!d.submenus[_data[this.index]][this.row-1][this.selected])  {
                this.index = 0;
                this.row = 0;
                this.selected = 0;
                return this.render()
            }
                    let str = d.submenus[_data[this.index]][this.row-1][this.selected]
                    d.submenus[_data[this.index]][this.row-1][this.selected] = [chalk.underline(str)]



                console.log(table.table(d.submenus[_data[this.index]]))
                console.log(this.selected)
            

        }
    }

    on(event, func)
    {
        this.events.on(event, func)
        
    }



}