
//requires the event file
const reqEvent = (event) => require (`../events/${event}`)
module.exports = client => {
  //I bet you can figure this out...
 client.on('ready', () => reqEvent('ready')(client));
 client.on('message', reqEvent('message'));
};
