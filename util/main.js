var fs = require("fs")
const readline = require('readline');
const chalk = require("chalk")
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal:false
   });
exports.newCommand = function(ui) {


     ui.pause(true)


       rl.question(chalk.underline("command name:"), (answer) => {

        const preset = `
        exports.run = function(client, message, args) {
    
        }
    
        module.exports.config = {
            name: "${answer}",
            aliases: [],
            usage: "USAGE",
            description:"desc",
        }
        `


        try {
            fs.writeFileSync(`./commands/${answer}.js`,preset)
            console.log(chalk.green(`Command ${answer} succesfully created!`))
        } catch(err) {
            console.log(`\n${chalk.red('ERROR:')} ${err}`)
        }
        
       
        rl.close;
        setTimeout(function(){
            ui.pause(false)
            ui.render()
        },3000)
       });
      

}

exports.dataEdit = function(message, ui) {
    ui.pause(true)
    rl.question(chalk.underline(`change the value of ${message[0]} (current value: ${message[1]}): `), (answer) => {
        // TODO: Log the answer in a database
        try {
            var data = JSON.parse(fs.readFileSync(`./util/config.json`, "utf8"))
            data[message[0]] = answer;
            fs.writeFileSync(`./util/config.json`, JSON.stringify(data))
            console.log(chalk.green(`data ${answer} succesfully saved!`))
        } catch(err) {
            console.log(`\n${chalk.red('ERROR:')} ${err}`)
        }
        
       
        rl.close;
        
        setTimeout(function(){
            ui.pause(false)
            ui.reload()
            ui.render()
        },1000)
        
       });

}

exports.load = function(client) { 
    var Discord = require("discord.js")
    client.commands = new Discord.Collection();
    client.aliases = new Discord.Collection();
    fs.readdir(`${__dirname}/../commands`, (err, files) => {
        if (err) client.ui.data.submenus.Logs.push([[`${chalk.red("ERROR")}`],[err.toString()]])

        client.ui.data.submenus.Logs.push([[`${chalk.blue("INFO")}`],[`performing (re)load of commands...`]])
        let jsfile = files.filter(f => f.split(".").pop() === "js")
        if (jsfile.length <= 0) {
            client.ui.data.submenus.Logs.push([[`${chalk.yellow("CAUTION")}`],[`no commands found`]])
        }
        jsfile.forEach((f, i) => {
            client.ui.data.submenus.Logs.push([[`${chalk.blue("INFO")}`],[`command ${f} loaded...`]])
            delete require.cache[require.resolve(`${__dirname}/../commands/${f}`)]
          let pull = require(`${__dirname}/../commands/${f}`);
          client.commands.set(pull.config.name, pull)
          pull.config.aliases.forEach(alias => {
            client.aliases.set(alias, pull.config.name)
          })
        })
      })
      

}
