
const chalk = require("chalk")
module.exports = message => {
    
    const client = message.client;
    var prefix = client.ui.config.CONF_prefix
    if(message.channel.type == "dm") return;
    if(message.author.bot) return;
    if(message.content.indexOf(prefix) != 0) return;
    
    if(!message.channel.permissionsFor(message.guild.me).has("SEND_MESSAGES")) return
    var args = message.content.slice(prefix.length).trim().split(/ +/g);
    var command = args.shift().toLowerCase();
    
    let commandfile = client.commands.get(command) || client.commands.get(client.aliases.get(command))
  if (!commandfile) return;
  try {
    let cmdFile = require(`../commands/${commandfile.config.name}`);
    cmdFile.run(client, message, args);
  } catch (err) {
    client.ui.data.submenus.Logs.push([[`${chalk.red("ERROR")}`],[`ERROR FOR COMMAND ${commandfile.config.name}: ${err.toString()} `]])
  }


}