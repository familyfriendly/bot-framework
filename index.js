var u = require("./util/ui.js");
var util = require("./util/main.js")
var ui = new u.ui();
var chalk = require("chalk")
var table = require("table")
const Discord = require("discord.js");
var fs = require("fs")
var client = new Discord.Client()
util.load(client)
require('./util/eventLoader.js')(client)
ui.start();
function loadEss()
{
    ui.data.submenus.Logs.push([[`TYPE`],["MESSAGE"]])
    ui.data.submenus.Logs.push([[`${chalk.red("CLEAR")}`],[`${chalk.green("SAVE")}`]])
}
loadEss()
ui.on("return", (Message) => {

if(Message.selected_row.item == "\u001b[31mCLEAR\u001b[39m") {
    ui.data.submenus.Logs = [];
    loadEss()
    ui.render()
}
if(Message.selected_row.item == "\u001b[32mSAVE\u001b[39m") {
fs.writeFileSync("./logs.txt",table.table(ui.data.submenus.Logs))
}
if(Message.selected_row.item == "exit") process.exit();
if(Message.selected_row.item == "command") util.newCommand(ui)
if(Message.selected_row.row[0] && String(Message.selected_row.row[0]).startsWith("CONF_")) {
    util.dataEdit(Message.selected_row.row[0],ui)
}
if(Message.selected_row.item == "Start bot") {
    login()
}
if(Message.selected_row.item == "Reload") util.load(client)
})
client.ui = ui

if(client.ui.config.CONF_startonload && client.ui.config.CONF_startonload == "true") login()

function login() {
    try {
        client.login(ui.config.CONF_token)
        .catch(err => ui.data.submenus.Logs.push([[`${chalk.red("ERROR")}`],[err.toString()]]))
    } catch (err) {
        ui.data.submenus.Logs.push([[`${chalk.red("ERROR")}`],[err.toString()]])
    }
}


